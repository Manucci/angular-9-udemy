import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListPokemonComponent} from './pokemons/list-pokemon/list-pokemon.component';
import {DetailPokemonComponent} from './pokemons/detail-pokemon/detail-pokemon.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';


const routes: Routes = [
  {path: '', redirectTo: 'pokemons', pathMatch: 'full'},
  {path: '**', redirectTo: '/404'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[pkmnBorderCard]'
})
export class BorderCardDirective {
  constructor(private el: ElementRef) {
    this.setBorder(this.INITIAL_COLOR);
    this.setHeight(this.DEFAULT_HEIGHT);
  }

  private INITIAL_COLOR: string = '#f5f5f5';

  private INITIAL_TRANSITION: string = '0';
  private INITIAL_BACKGROUND_COLOR: string = 'white';
  private DEFAULT_COLOR: string = '#009688';

  private DEFAULT_HEIGHT: number = 180;
  private DEFAULT_TRANSITION: string = '2000';
  private DEFAULT_BACKGROUND_COLOR: string = '#F0FFFF';

  @Input('pkmnBorderCard') borderColor: string;

  @HostListener('mouseenter') onMouseEnter() {
    this.setTransition(this.DEFAULT_TRANSITION);
    this.setBackgroundColor(this.DEFAULT_BACKGROUND_COLOR);
    this.setBorder(this.borderColor || this.DEFAULT_COLOR);
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.setTransition(this.INITIAL_TRANSITION);
    this.setBackgroundColor(this.INITIAL_BACKGROUND_COLOR);
    this.setBorder(this.INITIAL_COLOR);
  }

  private setTransition(transition: string) {
    this.el.nativeElement.style.transition = transition + 'ms';
  }

  private setBackgroundColor(color: string) {
    this.el.nativeElement.style.backgroundColor = color;
  }

  private setBorder(color: string) {
    const border = 'solid 4px ' + color;
    this.el.nativeElement.style.border = border;
  }

  private setHeight(height: number) {
    this.el.nativeElement.style.height = height + 'px';
  }
}

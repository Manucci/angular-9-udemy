import {Injectable} from '@angular/core';
import {Pokemon} from './pokemon';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PokemonsService {

  constructor(private http: HttpClient) {
  }

  POKEMON_URL: string = '/api/';


  getPokemons(): Observable<Pokemon[]> {
    return this.http.get<Pokemon[]>(this.POKEMON_URL+'pokemons').pipe(
      tap(_ => this.log(`fetched pokemons`)),
      catchError(this.handleError(`getPokemons`, [])));

  }

  private log(log: string) {
    console.info(log);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(error);
      console.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  getPokemon(id: number): Observable<Pokemon> {
    const url = `${this.POKEMON_URL + 'pokemons'}/${id}`;

    return this.http.get<Pokemon>(url)
      .pipe(tap(_ => this.log('fetched pokemon id=${id}')),
        catchError(this.handleError<Pokemon>(`getPokemon`)));
  }

  getPokemonTypes(): string[] {
    return ['Plante', 'Feu', 'Eau', 'Insecte', 'Normal', 'Electrik', 'Poison', 'Fée', 'Vol'];
  }
}

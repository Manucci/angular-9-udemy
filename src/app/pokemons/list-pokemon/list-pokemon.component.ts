import {Component, OnInit} from '@angular/core';
import {Pokemon} from '../pokemon';
import {Router} from '@angular/router';
import {PokemonsService} from '../pokemons.service';


@Component({
  selector: 'app-list-pokemon',
  templateUrl: './list-pokemon.component.html',
  styleUrls: ['./list-pokemon.component.sass']
})
export class ListPokemonComponent implements OnInit {

  pokemons: Pokemon[];

  constructor(private router: Router, private pokemonsService: PokemonsService) {
  }

  ngOnInit(): void {
    this.getPokemons();
  }

  getPokemons(): void {

    let pokemons1 = this.pokemonsService.getPokemons();
    pokemons1.subscribe(pokemons => this.pokemons = pokemons);
    console.log(this.pokemons);
  }

  selectPokemon(pokemon: Pokemon): void {
    console.log('Vous avez cliqué sur {{pokemon.name}}');
    const link = ['/pokemon/', pokemon.id];
    this.router.navigate(link);
  }

}

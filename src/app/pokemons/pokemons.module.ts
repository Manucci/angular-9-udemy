import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DetailPokemonComponent} from './detail-pokemon/detail-pokemon.component';
import {ListPokemonComponent} from './list-pokemon/list-pokemon.component';
import {PokemonTypeColorPipe} from './pokemon-type-color.pipe';
import {BorderCardDirective} from './border-card.directive';
import {PokemonRoutingModule} from './pokemons-routing.module';
import {PokemonsService} from './pokemons.service';
import { PokemonFormComponent } from './pokemon-form/pokemon-form.component';
import { EditPokemonComponent } from './edit-pokemon/edit-pokemon.component';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [DetailPokemonComponent,
    ListPokemonComponent,
    PokemonTypeColorPipe,
    BorderCardDirective,
    PokemonFormComponent,
    EditPokemonComponent],
  imports: [
    CommonModule,
    FormsModule,
    PokemonRoutingModule
  ],
  providers: [PokemonsService]
})
export class PokemonsModule {
}

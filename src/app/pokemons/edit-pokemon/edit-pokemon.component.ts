import {Component, OnInit} from '@angular/core';
import {Pokemon} from '../pokemon';
import {PokemonsService} from '../pokemons.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-edit-pokemon',
  templateUrl: './edit-pokemon.component.html',
  styleUrls: ['./edit-pokemon.component.sass']
})
export class EditPokemonComponent implements OnInit {

  pokemon: Pokemon = null;

  constructor(
    private route: ActivatedRoute,
    private pokemonsService: PokemonsService) {
  }

  ngOnInit(): void {
    const id = +this.route.snapshot.params['id'];
    this.pokemonsService.getPokemon(id)
      .subscribe(pokemon => this.pokemon);
  }

}
